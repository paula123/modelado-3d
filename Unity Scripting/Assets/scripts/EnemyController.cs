﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    private CharacterHealth characterHealth;
    public int points; 

	void Start () {
        characterHealth = GetComponent<CharacterHealth>();
	}
	
	void Update () {

        if (characterHealth.isDead)
        {
            GameController.score += points;
            Destroy(gameObject, 0.2f);
            enabled = false;
        }
	}
}
